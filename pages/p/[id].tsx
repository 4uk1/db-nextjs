import React from "react"
import prisma from "../../lib/prisma" // db interface
import { GetServerSideProps } from "next"
import Router from 'next/router';
import { useSession } from 'next-auth/client';
import ReactMarkdown from "react-markdown"
import Layout from "../../components/Layout"
import { PostProps } from "../../components/Post"

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  // const post = {
  //   id: 1,
  //   title: "Prisma is the perfect ORM for Next.js",
  //   content: "[Prisma](https://github.com/prisma/prisma) and Next.js go _great_ together!",
  //   published: false,
  //   author: {
  //     name: "Nikolas Burk",
  //     email: "burk@prisma.io",
  //   },
  // }
  const post = await prisma.post.findUnique({
    where: {
      id: Number(params?.id) || -1,
    },
    include: {
      author: {
        select: { name: true },
      },
    },
  })

  return { props: post }
}

async function publishPost(id: number): Promise<void> {
  await fetch(`${location.origin}/api/publish/${id}`, {
    method: 'PUT',
  });

  await Router.push('/');
}

async function deletePost(id: number): Promise<void> {
  await fetch(`${location.origin}/api/post/${id}`, {
    method: 'DELETE',
  });

  Router.push('/');
}

const Post: React.FC<PostProps> = (props) => {
  const [session, loading] = useSession();
  if (loading) {
    return <div>Authenticating ...</div>;
  }

  const userHasValidSession = Boolean(session);
  // const postBelongsToUser = session?.user?.email === props.author?.email;
  const postBelongsToUser = session?.user?.name === props.author?.name;

  let title = props.title
  if (!props.published) {
    title = `${title} (Draft)`
  }

  // console.log('drafts:', props);
  // console.log('published?', props.published);
  // console.log('user session valid?', userHasValidSession)
  // console.log('user owns post?', postBelongsToUser);

  return (
    <Layout>
      <div>
        <h2>{title}</h2>
        <p>By {props?.author?.name || "Unknown author"}</p>

        <ReactMarkdown source={props.content} />

        {!props.published && userHasValidSession && postBelongsToUser && (
          <button onClick={() => publishPost(props.id)}>Publish</button>
        )}

        {userHasValidSession && postBelongsToUser && (
          <button onClick={() => deletePost(props.id)}>Delete</button>
        )}
      </div>
      <style jsx>{`
        .page {
          background: white;
          padding: 2rem;
        }

        .actions {
          margin-top: 2rem;
        }

        button {
          background: #ececec;
          border: 0;
          border-radius: 0.125rem;
          padding: 1rem 2rem;
        }

        button + button {
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  )
}

export default Post
