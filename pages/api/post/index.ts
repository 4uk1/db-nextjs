import { getSession } from 'next-auth/client';
import prisma from '../../../lib/prisma';

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { title, content } = req.body;

  const session = await getSession({ req });

  const name = session?.user?.name;
  const user = await prisma.user.findFirst({
    where: {
      name: { equals: name }
    }
  })

  const result = await prisma.post.create({
    data: {
      title: title,
      content: content,
      authorId: user.id,
    },
  });

  res.json(result);
}
