import { useState } from "react";
import { signIn, getSession } from "next-auth/client";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // "username-login" matches the id for the credential
    signIn(
      "username-login",
      { 
        username,
        password,
        callbackUrl: '/'
      }
    ).then(
      (res) => {
        console.log('login done:', res);
      }
    );
  };

  return (
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="username">Username</label>
            <input
              id="username"
              name="username"
              type="text"
              placeholder="Username"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
            />
          </div>
          <div>
            <label htmlFor="password">Password</label>
            <input
              id="password"
              name="password"
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
          </div>
          <button type="submit">
            Login
          </button>
        </form>
  );
}
